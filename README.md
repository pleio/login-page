# Login Page

A bare bones login page that can use an external OIDC provider and will redirect users to a different website after logging in. This can be used in cases where we want to avoid the regular login flow from backend2.

## Getting started

1. Copy the `.env-example` file to `.env` and fill in the missing values.
2. Run `docker-compose up`
    a. You might need to create the `database` and `static` folder and set `www-data` as the user in case there are permission errors.
3. The login page should be accessible at http://localhost:8002


