import string
import random
from mozilla_django_oidc.auth import OIDCAuthenticationBackend


class OIDCAuthBackend(OIDCAuthenticationBackend):

    def get_or_create_user(self, access_token, id_token, payload):
        username = ''.join(random.choices(
            string.ascii_uppercase + string.digits, k=12))
        return self.UserModel.objects.create_user(username)
