from django.shortcuts import render, redirect
from django.http import HttpResponse


def home(request):
    return render(request, 'home.html', {})


def unmatched(request):
    return redirect('home')


def health(request):
   return HttpResponse('ok', status=200)