{{- define "login.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end -}}

{{- define "login.secretsName" -}}
{{- printf "%s-secrets" ((include "login.name" .)) -}}
{{- end -}}

{{- define "login.tlsSecretName" -}}
{{- printf "tls-%s" ((include "login.name" .)) -}}
{{- end -}}

{{- define "login.frontendName" -}}
{{- printf "%s-frontend" ((include "login.name" . )) -}}
{{- end -}}

{{- define "login.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "login.labels" -}}
helm.sh/chart: {{ include "login.chart" . }}
{{ include "login.selectorLabels" . }}
{{- end -}}

{{- define "login.selectorLabels" -}}
app.kubernetes.io/name: {{ include "login.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Component }}
app.kubernetes.io/component: {{ .Component }}
{{- end }}
{{- end -}}
