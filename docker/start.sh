#!/usr/bin/env sh

# Collect static files
python /app/manage.py collectstatic --noinput

# Apply database migrations
python /app/manage.py migrate

# Start uWSGI
echo "Starting uWSGI..."
uwsgi --ini /app/docker/uwsgi.ini
