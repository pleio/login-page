# Use Alpine as the base for the build stage
FROM python:3.12-alpine AS build

# Install build dependencies
RUN apk add --no-cache \
    build-base \
    linux-headers \
    pcre-dev  # uwsgi requires PCRE for building

WORKDIR /app
COPY requirements.txt requirements.txt

# Install Python dependencies into a separate directory
RUN pip3 install --no-cache-dir --prefix=/install -r requirements.txt

# Final stage using Alpine
FROM python:3.12-alpine

# Install runtime dependencies
# Install build dependencies
RUN apk add --no-cache \
    pcre  # Needed for uwsgi runtime

# Copy installed Python packages from the build stage
COPY --from=build /install /usr/local

WORKDIR /app
COPY . .

# Create necessary directories and set ownership
RUN mkdir -p static static-frontend database && \
    chown -R nobody:nobody static static-frontend database

# Expose the application port
EXPOSE 8000

# Switch to a non-root user
USER nobody
